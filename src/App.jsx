import { useEffect, useState } from "react";
import { v4 as uuidv4 } from "uuid";

function App() {
  const [input, setInput] = useState({
    amount: 0,
    description: "",
    type: "expense",
  });

  const [balance, setBalance] = useState({
    balance: 0,
    expense: 0,
    income: 0,
  });

  const [showError, setShowError] = useState({
    amount: false,
    description: false,
  });

  const [transaction, setTransaction] = useState([]);

  const [tab, setTab] = useState(1);

  useEffect(() => {
    calculateBalance();
  }, [transaction]);

  const calculateBalance = () => {
    let balanceSum = 0;
    let expenseSum = 0;
    let incomeSum = 0;
    transaction.map((item) => {
      if (item.transactionType === "expense") {
        expenseSum = Number(expenseSum) + Number(item.amount);
        balanceSum = Number(balanceSum) - Number(item.amount);
      }
      if (item.transactionType === "income") {
        incomeSum = Number(incomeSum) + Number(item.amount);
        balanceSum = Number(balanceSum) + Number(item.amount);
      }
    });
    setBalance({
      balance: balanceSum,
      expense: expenseSum,
      income: incomeSum,
    });
  };

  const handleAddNewRecord = () => {
    const { description, amount, type } = input;

    let date = new Date();

    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();

    let currentDate = `${year}-${month}-${day}`;

    if (!amount) {
      return setShowError({
        description: false,
        amount: true,
      });
    } else if (!description) {
      return setShowError({
        description: true,
        amount: false,
      });
    } else {
      setShowError({
        description: false,
        amount: false,
      });
      setTransaction([
        ...transaction,
        {
          id: uuidv4(),
          amount: amount,
          description: description,
          transactionType: type,
          date: currentDate,
        },
      ]);
      clearInput();
      console.log(transaction);
    }
  };

  const handleUpdateInput = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };

  const clearInput = () => {
    setInput({
      amount: 0,
      description: "",
      type: "expense",
    });
  };

  const renderTransaction = () => {
    return transaction.toReversed().map((item) => (
      <div key={item.id} className="stats shadow group hover:shadow-md">
        <div
          className={
            "stat w-full " +
            (item.transactionType === "expense" ? "bg-primary" : "bg-secondary")
          }
        >
          <div className="stat-title font-bold flex flex-row justify-between">
            <div className="capitalize text-slate-900">{item.description}</div>
            <div className="flex flex-row gap-2 invisible group-hover:visible">
              <button className="btn btn-square btn-xs">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                  />
                </svg>
              </button>
              <button className="btn btn-square btn-xs">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                  />
                </svg>
              </button>
            </div>
          </div>
          <div className="stat-value text-2xl text-right">{item.amount}</div>
          <div className="stat-description font-medium">{item.date}</div>
        </div>
      </div>
    ));
  };

  return (
    <main className="flex flex-col gap-6 max-w-2xl mx-auto py-10 px-4">
      <h1 className="font-black text-3xl text-center">My Expense Tracker</h1>
      <div className="stats shadow">
        <div className="stat bg-primary w-full">
          <div className="stat-title font-bold">Expense</div>
          <div className="stat-value text-2xl text-right">
            - {balance.expense}
          </div>
        </div>
        <div className="stat bg-secondary w-full">
          <div className="stat-title font-bold">Income</div>
          <div className="stat-value text-2xl text-right">
            + {balance.income}
          </div>
        </div>
      </div>
      <div
        className={
          "stats shadow " +
          (balance.balance > 0 ? "bg-secondary" : "bg-primary")
        }
      >
        <div className="stat w-full">
          <div className="stat-title font-bold">Balance</div>
          <div className="stat-value text-2xl text-right">
            {balance.balance > 0 && "+ "}
            {balance.balance}
          </div>
        </div>
      </div>

      <div
        role="tablist"
        className="tabs tabs-lifted [&>a]:text-md [&>a]:font-bold [&>a]:text-gray-600"
      >
        <a
          role="tab"
          className={"tab " + (tab === 1 && "tab-active")}
          onClick={() => setTab(1)}
        >
          History
        </a>
        <div
          className={
            "tab-content bg-base-100 border-base-300 rounded-box p-10 " +
            (tab !== 1 && "hidden")
          }
        >
          <div className="flex flex-col gap-3">{renderTransaction()}</div>
        </div>
        <a
          role="tab"
          className={"tab " + (tab === 2 && "tab-active")}
          onClick={() => setTab(2)}
        >
          Input
        </a>
        <div
          className={
            "tab-content bg-base-100 border-base-300 rounded-box p-10 " +
            (tab !== 2 && "hidden")
          }
        >
          <div className="flex flex-col gap-2">
            <div className="join">
              <select
                className="select select-bordered join-item w-1/2 font-bold text-lg"
                onChange={handleUpdateInput}
                value={input.type}
                name="type"
              >
                <option
                  className="bg-primary font-bold text-lg"
                  value="expense"
                >
                  Expense
                </option>
                <option
                  className="bg-secondary font-bold text-lg"
                  value="income"
                >
                  Income
                </option>
              </select>
              <input
                className={
                  "input input-bordered join-item w-1/2 text-right font-bold text-lg placeholder:text-left " +
                  (showError.amount && "bg-error")
                }
                type="number"
                placeholder="Amount"
                onChange={handleUpdateInput}
                value={input.amount}
                name="amount"
              />
            </div>
            <input
              className={
                "input input-bordered join-item font-bold text-lg " +
                (showError.description && "bg-error")
              }
              placeholder="Description"
              onChange={handleUpdateInput}
              value={input.description}
              name="description"
            />
            <div className="join">
              <button className="btn join-item w-1/4" onClick={clearInput}>
                Clear
              </button>
              <button
                className="btn btn-neutral join-item w-3/4"
                onClick={handleAddNewRecord}
              >
                Record
              </button>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}

export default App;
